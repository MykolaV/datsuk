<?php

namespace App\Http\Controllers;

use App\Product;
use App\Section;
use App\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::active()->ordered()->get();
        $sections = Section::all();
        $settings = Setting::all();
        return view('front.pages.home.index', compact('products', 'sections', 'settings'));
    }

    public function callCounter()
    {
        $callCounter = Setting::firstOrNew(['title' => 'callCount']);
        $callCounter->value++;
        $callCounter->save();
    }
}
