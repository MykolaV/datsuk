<?php

namespace App\Providers;

use App\Cart;
use App\Delivery;
use App\Establishment;
use App\Setting;
use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['front.layouts.inc.begin'], function ($view) {
            $settings = Setting::all();

            $view->with([
                'settings' => $settings
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
