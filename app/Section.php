<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Section extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'title',
        'description',
        'is_active'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('main')->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_FILL, 50, 50)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('320x320')
            ->fit(Manipulations::FIT_CONTAIN, 320, 320)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('webp')
            ->format('webp')
            ->nonQueued();
    }
}
