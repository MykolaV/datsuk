<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ofcold\NovaSortable\SortableTrait;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SortableTrait;

    protected $fillable = [
        'title',
        'description',
        'weight',
        'price',
        'is_active'
    ];

    public static function orderColumnName(): string
    {
        return 'position_order';
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('position_order', 'ASC');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('main')->singleFile();
//        $this->addMediaCollection('modal')->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_FILL, 50, 50)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('320x320')
            ->fit(Manipulations::FIT_CONTAIN, 320, 320)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('360x240')
            ->fit(Manipulations::FIT_CONTAIN, 360, 240)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('960x600')
            ->fit(Manipulations::FIT_CONTAIN, 960, 600)
            ->format('webp')
            ->nonQueued();
    }
}
