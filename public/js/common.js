$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".callback-bt").click(function(){
        var PhoneNumber = $(this).data('phone');
        PhoneNumber=PhoneNumber.replace("Phone:","");
        window.location.href="tel://"+PhoneNumber;

        var route = $(this).data('route');

        $.ajax({
            method: 'POST',
            url: route,
            dataType: 'json',
            data: { data: ''},
            success: function(data) {
                //
            },
            error: function() {
                console.log('Error Ajax!')
            }
        })
    });
})
