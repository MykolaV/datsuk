<section class="paralax-mf footer-paralax bg-image sect-mt4 route" style="background-image: url({{ $contactsSection->getFirstMediaUrl('main', 'webp') }})">
    <div class="overlay-mf"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="contact-mf">
                    <div id="contact" class="box-shadow-full">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="title-box-2">
                                    <h5 class="title-left">
                                        Напишіть нам
                                    </h5>
                                </div>
                                <div>
                                    <form action="" method="post" role="form" class="contactForm">
                                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                                        <div id="errormessage"></div>
                                        <div class="row">
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="text" name="name" class="form-control" id="name" placeholder="Ім'я" data-rule="minlen:4" data-msg="Please enter at least 4 chars" disabled/>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="phone" id="email" placeholder="Телефон" data-rule="email" data-msg="Please enter a valid email" disabled/>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Повідомлення" disabled></textarea>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="button button-a button-big button-rouded">Відправити</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="title-box-2 pt-4 pt-md-0">
                                    <h5 class="title-left">
                                        Зв'яжіться з нами
                                    </h5>
                                </div>
                                <div class="more-info">
                                    <p class="lead">
                                        {{ $contactsSection->description }}
                                    </p>
                                    <ul class="list-ico">
                                        <li><br></li>
                                        <li><span class="ion-ios-telephone"></span> {{ optional($settings->firstWhere('title', 'phone'))->value }}</li>
                                        <li><span class="ion-email"></span> {{ optional($settings->firstWhere('title', 'email'))->value }}</li>
                                    </ul>
                                </div>
                                <div class="socials">
                                    <ul>
                                        <li><a href=""><span class="ico-circle"><i class="ion-social-facebook"></i></span></a></li>
                                        <li><a href=""><span class="ico-circle"><i class="ion-social-instagram"></i></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('front.layouts.inc.footer')
</section>
