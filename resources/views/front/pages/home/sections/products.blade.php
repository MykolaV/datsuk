<section id="service" class="portfolio-mf sect-pt4 route">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="title-box text-center">
                    <h3 class="title-a">
                        Будівельні матеріали
                    </h3>
                    <p class="subtitle-a">
                        Гарантуємо якість усіх матеріалів.
                    </p>
                    <div class="line-mf"></div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-md-4">
                    <div class="work-box">
                        <a href="{{ $product->getFirstMediaUrl('main', '960x600') }}" data-lightbox="gallery-mf">
                            <div class="work-img">
                                <img src="{{ $product->getFirstMediaUrl('main', '360x240') }}" alt="" class="img-fluid">
                            </div>
                            <div class="work-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2 class="w-title"><b>{{ $product->title }}</b></h2>
                                        <p>{{ $product->description }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="w-more">
                                <span class="w-ctegory">{{ $product->weight }}</span> /
                                <span class="w-date">{{ $product->price }} грн</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
