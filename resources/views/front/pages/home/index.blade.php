@extends('front.layouts.app')

@section('content')
    <!--/ Nav Star /-->
    <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll" href="#page-top">Datsuk-truns</a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
                    aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link js-scroll" href="#about">Про компанію</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll" href="#service">Послуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll" href="#contact">Контакти</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--/ Nav End /-->

    <!--/ Intro Skew Star /-->
    @if($headerSection = $sections->where('slug', 'header')->first())
        <div id="home" class="intro route bg-image" style="background-image: url({{ $headerSection->getFirstMediaUrl('main', 'webp') }})">
            <div class="overlay-itro"></div>
            <div class="intro-content display-table">
                <div class="table-cell">
                    <div class="container">
                        <!--<p class="display-6 color-d">Hello, world!</p>-->
                        <h1 class="intro-title mb-4">{{ $headerSection->title }}</h1>
                        <p class="intro-subtitle"><span class="text-slider-items">{{ $headerSection->description }}</span><strong class="text-slider"></strong></p>
                        <!-- <p class="pt-3"><a class="btn btn-primary btn js-scroll px-4" href="#about" role="button">Learn More</a></p> -->
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($aboutSection = $sections->where('slug', 'about')->first())
        <section id="about" class="about-mf sect-pt4 route">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-shadow-full">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src="{{ $aboutSection->getFirstMediaUrl('main', 'webp') }}" class="img-fluid rounded b-shadow-a" alt="">
                                </div>
                                <div class="col-md-6">
                                    <div class="about-me pt-4 pt-md-0">
                                        <div class="title-box-2">
                                            <h5 class="title-left">
                                                {{ $aboutSection->title }}
                                            </h5>
                                        </div>
                                        <p class="lead">
                                            {!! $aboutSection->description !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @include('front.pages.home.sections.products', compact('products'))

    @if($contactsSection = $sections->where('slug', 'contacts')->first())
        @include('front.pages.home.sections.form', compact('contactsSection', 'settings'))
    @endif
    <div id="preloader"></div>
    <div type="button" class="callback-bt" data-phone="{{ optional($settings->firstWhere('title', 'phone'))->value }}" data-route="{{ route('call.counter') }}">
        <div class="text-call">
            <i class="fa fa-phone"></i>
        </div>
    </div>
@endsection
