<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, viewport-fit=cover"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta http-equiv="cleartype" content="on"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="address=no"/>

    <title>
        {{ optional($settings->firstWhere('title', 'seo_title'))->value }}
    </title>
    <meta name="keywords" content="{{ optional($settings->firstWhere('title', 'seo_keywords'))->value }}"/>
    <meta name="description" content="{{ optional($settings->firstWhere('title', 'seo_description'))->value }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content=""/>
    <meta name="copyright" content="(c)">
    <meta http-equiv="Reply-to" content="noreply@suliguli.com">

    <meta name="theme-color" content="#000000">


    <!-- Twitter card -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@twitter" />
    <meta name="twitter:creator" content="@twitter" />
    <meta name="twitter:image" content="https://example.com/example.png" />
    <meta name="twitter:description" content="{{ optional($settings->firstWhere('title', 'seo_description'))->value }}" />
    <meta name="twitter:title" content="{{ optional($settings->firstWhere('title', 'seo_title'))->value }}" />


    <!-- Open Graph -->
    <meta property="og:title" content="{{ optional($settings->firstWhere('title', 'seo_title'))->value }}" />
    <meta property="og:description" content="{{ optional($settings->firstWhere('title', 'seo_description'))->value }}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="https://example.com/example1200x630.png" />
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:site_name" content="Datsuk Truns" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS File -->
    <link href="/front/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="/front/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/front/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/front/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/front/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/front/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="/front/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/common.css" inline>
</head>
<body  id="page-top">

