<!-- JavaScript Libraries -->
<script src="/front/lib/jquery/jquery.min.js"></script>
<script src="/front/lib/jquery/jquery-migrate.min.js"></script>
<script src="/front/lib/popper/popper.min.js"></script>
<script src="/front/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/front/lib/easing/easing.min.js"></script>
<script src="/front/lib/counterup/jquery.waypoints.min.js"></script>
<script src="/front/lib/counterup/jquery.counterup.js"></script>
<script src="/front/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/front/lib/lightbox/js/lightbox.min.js"></script>
<script src="/front/lib/typed/typed.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="/front/contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="/front/js/main.js"></script>
<script src="/js/common.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-165878679-1">
</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-165878679-1');
</script>
</body>
</html>
