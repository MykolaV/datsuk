<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="copyright-box">
                    <p class="copyright">&copy; 2020 Datsuk Truns</p>
                </div>
            </div>
        </div>
    </div>
</footer>
