<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::firstOrCreate([
            'title' => 'facebook',
        ], [
            'value' => 'https://www.facebook.com/'
        ]);

        Setting::firstOrCreate([
            'title' => 'instagram',
        ], [
            'value' => 'https://www.instagram.com/'
        ]);

        Setting::firstOrCreate([
            'title' => 'email',
        ], [
            'value' => 's.datsuk@gmail.com'
        ]);

        Setting::firstOrCreate([
            'title' => 'phone',
        ], [
            'value' => '+380 50 991-07-85'
        ]);

        Setting::firstOrCreate([
            'title' => 'notification_email',
        ], [
            'value' => ''
        ]);

        \App\Setting::firstOrCreate([
            'title' => 'seo_keywords',
        ], [
            'value' => 'Щебінь купити, пісок, щебінь, Оптовий продаж будівельних матеріалів, відсів, Вивіз будівельного сміття, Чорнозем купити, пісок купити, доставка щебеню, чорнозем, достувка піску у луцьку, пісок луцьк, щебінь луцьк, чорнозем луцьк, купти чорнозем луцьк, купити чорнозем ціна, купити чорнозем,'
        ]);

        \App\Setting::firstOrCreate([
            'title' => 'seo_title',
        ], [
            'value' => 'Datsuk Truns - вантажні перевезення'
        ]);

        \App\Setting::firstOrCreate([
            'title' => 'seo_description',
        ], [
            'value' => 'Доставка будівельних матеріалів, торфу, чорнозему, дров. Послуги Bobcat'
        ]);
    }
}
